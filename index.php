<!DOCTYPE html>
<html lang="en">
<head>
	<?php 
		include('include/css.php');

	?>
	<title>My Cover Letter</title>

</head>
<body>
	<div class="container">
		<div class="row">
			<!-- start col-md-12 -->
			<div class="col-md-12">
				<!-- header -->
				<div class="header">
					<legend><h1>My Cover Letter</h1></legend>
					<button class="btn btn-success pull-right" data-toggle="modal" data-target="#addModal">Add Cover Letter</button>
				</div>
				<div class="clearfix"></div>
				<hr>
				<!-- end of header -->

				<!-- list of letter -->
				<ul id="list-letter" class="panel list-group-item-success list-unstyled">
				</ul>
				<!-- end list of letter -->
				<div id="marker-end"></div>
			</div>
			<!-- end of col-md-12 -->
		</div>
	</div>
	<?php
		include('include/modal.php');
	?>
	<!-- footer -->
	<footer class="navbar-fixed-bottom footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<small>Copyright 2016. All Right Reserved</small>
					
				</div>
			</div>
		</div>
	</footer>
	<?php 
		include('include/js.php'); 
	?>
	<!-- end of footer -->
</body>
</html>