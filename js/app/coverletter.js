var coverletterManagement = (function($) {
    var apiUrl = 'http://localhost/bitbucket/exam/api/v1/public/'
    var index = 1;

    return {
        init: init
    };

    // Init
    function init() {
        addEventHandlers();
    }

    // Event Handler
    function addEventHandlers() {

        setTimeout(function() {
            $('#marker-end').on('lazyshow', letterList).lazyLoadXT({visibleOnly: false});
        }, 1000);

        $("#addLetter").on('click', addLetter);
        $("#list-letter").on('click', 'a.deleteLetter', delete_letter);
        $("#list-letter").on('click', 'a.default-coverletter', default_letter);

        $("#list-letter").on('click', 'a.editLetter', edit_letter);

    }

    function edit_letter() {
       var letterId = $(this);
       $.ajax({
           type : 'GET',
           url : apiUrl + 'coverletter/' + letterId.data('edit'),
           success: function(response) {
               var title   = $('input[name=editTitle]');
               var content = $('textarea[name=editContent]');
               var hiddenId = $('input[name=editLetterId]');

               $.each(response.data, function(index, element) {
                   title.val(element.title);
                   content.val(element.content);
                   hiddenId.val(element.id);
               });

               // Confirm Modal Box
               $('#editConfirm').click(function(){

                   var data = {
                       title   : title.val(),
                       content : content.val(),
                       letter_id : hiddenId.val()
                   };

                   $.ajax({
                       type : 'POST',
                       url  : apiUrl + 'coverletter/update',
                       data : data,
                       cache: false,
                       success: function(response) {
                           if(response.success == true) {
                               pageReload();
                           } else { 
                               var errorMsg = $('.list-warning');

                               errorMsg.empty();

                               if(response.validation == false) {

                                   $.each(response.data, function(index, element) {
                                       errorMsg.append('<li class="leftmargin-sm">' + element + '</li>');
                                   });

                               } else {
                                   errorMsg.append('<li class="leftmargin-sm">Please contact the Support Team.</li>');
                               }

                               $('.panel-add-error-container').removeClass('hidden').fadeIn();

                               setTimeout(function() {
                                   $('.panel-add-error-container').fadeOut().addClass('hidden');
                               }, 10000);
                           }
                       }
                   })
               });
           }
       })
    }
    function delete_letter() {
        event.preventDefault();
        var letterId = $(this);
        $('#deleteConfirm').click(function() {
            $.ajax({
                type : 'DELETE',
                url  : apiUrl + 'coverletter/' + letterId.data('delete'),
                success: function(response) {
                    if(response.success == true) {
                        letterId.parent().parent().parent().fadeOut().remove();
                        $('#deleteModal').modal('hide');
                    }
                }
            })
        });
    }
    function letterList(){
        var listLetter = $('#list-letter');
        
        $.ajax({
                type : 'GET',
                url  : apiUrl + 'coverletter/page/' + index,
            }).done(function (response) {
                if(response.success == true) {
                    coverletter_len = response.data_len;

                    // Add Elements
                    $.each(response.data, function(index, element) {
                        if(element.is_default == 1) {
                          className = "panel-danger";
                        } else {
                          className = "panel-success";
                        }

                        listLetter.append('<li class="leftmargin-sm">\
                            <div class="panel ' + className + '">\
                                <div class="panel-heading">\
                                <a href="#" class="default-coverletter" data-default="' + element.id +'"  ><i class="fa fa-asterisk" aria-hidden="true"></i></a>\
                                    <strong>' + element.title + '</strong>\
                                <a href="#" class="deleteLetter" data-delete="' + element.id +'"  data-toggle="modal" data-target="#deleteModal">\
                                    <i class="fa fa-remove pull-right" aria-hidden="true"></i>\
                                </a>\
                                <a href="#" class="editLetter" data-edit="' + element.id +'" data-toggle="modal" data-target="#editModal">\
                                    <i class="fa fa-edit pull-right" aria-hidden="true"></i>\
                                </a>\
                            </div>\
                            <div class="panel-body">\
                                <p>'+ element.content +'</p>\
                            </div>\
                        </div></li>');
                    });

                    $(window).lazyLoadXT();
                    $('#marker-end').lazyLoadXT({visibleOnly: false, checkDuplicates: false});

                    // Increment Page
                    index++;
                } else {

                    $('#marker-end').addClass('hidden');

                }
            });
    }
    
    function pageReload(){
        location.reload();
    }

    function addLetter(){
        var title   = $('input[name=title]');
        var content = $('textarea[name=content]');

        var data = {
            title   : title.val(),
            content : content.val(),
        };

        $.ajax({
                type: 'POST',
                url: apiUrl + 'coverletter',
                data: data,
                cache: false,
                beforeSend: function(){
                    setTimeout(function() {
                        $('#addLetter').html('Saving');
                    }, 10000);
                   
                },
                success: function(response) {

                    $('#addLetter').val('Save changes');
                    if(response.success == true) {
                        title.val('');
                        content.val('');

                        setTimeout(function() {
                            $('#loading').addClass('hidden');
                            $('#list-view').removeClass('hidden');
                        }, 200);

                        pageReload();
                    } else {

                        var errorMsg = $('.list-warning');

                        errorMsg.empty();

                        if(response.validation == false) {

                            $.each(response.data, function(index, element) {
                                errorMsg.append('<li class="leftmargin-sm">' + element + '</li>');
                            });
                        } else {
                            errorMsg.append('<li class="leftmargin-sm">Please contact the Support Team.</li>');
                        }

                        $('.panel-add-error-container').removeClass('hidden').fadeIn();

                        setTimeout(function() {
                            $('.panel-add-error-container').fadeOut().addClass('hidden');
                        }, 10000);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    var errorMsg = $('.list-warning');

                    errorMsg.empty();

                    errorMsg.append('<li class="leftmargin-sm">Please contact the Support Team.</li>');

                    $('.panel-add-error-container').removeClass('hidden').fadeIn();

                    setTimeout(function() {
                        $('.panel-add-error-container').fadeOut().addClass('hidden');
                    }, 10000);
                }
            });
    }
  function default_letter(){
           var letterId = $(this);
             $.ajax({
               type : 'GET',
               url : apiUrl + 'coverletter/default/' + letterId.data('default'),
               success: function(response) {
                 console.log(response);
                 if(response.success == true) {

                     $(this).parent().parent().toggleClass('panel-danger');
                     setTimeout(function() {
                       pageReload();
                     }, 1000);

                 } else {
                   alert('There\'s an existing Default Cover Letter');
                 }

               }
             });
         }
})($);
$(coverletterManagement.init);