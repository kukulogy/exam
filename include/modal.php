<!-- Add Cover Letter
  ============================================= -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Add Cover Letter</h4>
      </div>

  
      <form id="addCoverLetter" method="POST">
     
          <div class="modal-body">
            <div class="panel panel-add-error-container panel-danger hidden ">
              <div class="panel-heading"><strong>Oops!</strong></div>

              <div class="panel-body">
                <ul class="list-warning nomargin">
                </ul>
              </div>
            </div>

            <div class="form-group">
               <label for="cover_letter_title">Cover Letter Title</label>
               <input type="text" name="title" class="form-control" id="cover_letter_title" placeholder="Title">
             </div>
             <div class="form-group">
               <label for="exampleInputPassword1">Content</label>
               <textarea class="form-control" name="content" id="" cols="30" rows="10"></textarea>
             </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="addLetter" class="btn btn-primary">Save changes</button>
          </div>
      </form>


    </div>
  </div>
</div>

<!-- Edit Cover Letter
	============================================= -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Edit Cover Letter</h4>
      </div>

  
      <form id="editCoverLetter" method="POST">
     
          <div class="modal-body">
            <div class="panel panel-add-error-container panel-danger hidden ">
              <div class="panel-heading"><strong>Oops!</strong></div>

              <div class="panel-body">
                <ul class="list-warning nomargin">
                </ul>
              </div>
            </div>
            <input type="hidden" name="editLetterId" />
            <div class="form-group">
               <label for="cover_letter_title">Cover Letter Title</label>
               <input type="text" name="editTitle" class="form-control" id="cover_letter_title" placeholder="Title">
             </div>
             <div class="form-group">
               <label for="exampleInputPassword1">Content</label>
               <textarea class="form-control" name="editContent" id="" cols="30" rows="10"></textarea>
             </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="editConfirm" class="btn btn-primary">Save changes</button>
          </div>
      </form>


    </div>
  </div>
</div>

<!-- Delete Cover Letter
  ============================================= -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Delete Confirmation</h4>
      </div>
          <div class="modal-body"><p>Are you sure you want delete?</p></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            <button type="button" id="deleteConfirm" class="btn btn-primary">Yes</button>
          </div>
      </form>


    </div>
  </div>
</div>