<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.lazyloadxt.js"></script>

<!-- jQuery Cookie Plugin -->
<script type="text/javascript" src="js/plugins/jquery.cookie.js"></script>



<!-- Bootstrap Plugins
============================================= -->
<script type="text/javascript" src="js/bootstrap.js"></script>


<!-- Bootstrap Data Table Plugin -->
<script type="text/javascript" src="js/components/bs-datatable.js"></script>

<!-- Include Underscore -->
<script type="text/javascript" src="js/underscore.js"></script>
<script type="text/javascript" src="js/app/coverletter.js"></script>
