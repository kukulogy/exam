<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Jobs Global" />

<!-- Stylesheets
============================================= -->
<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" href="css/dark.css" type="text/css" />
<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.css" type="text/css" />
<link rel="stylesheet" href="css/animate.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.css" type="text/css" />

<!-- Bootstrap Data Table Plugin -->
<link rel="stylesheet" href="css/components/bs-datatable.css" type="text/css" />

<link rel="stylesheet" href="css/custom.css" type="text/css" />

<link rel="stylesheet" href="css/responsive.css" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<style>

#marker-end {
    height: 32px;
    background: url(css/images/loading.gif) no-repeat 50% 50%;
}
.footer{
	width: 100%;
	margin-top: 10em;
	height: 50px;
	background-color: #f5f5f5;
}
.list-warning{
	list-style-position: outside;
	list-style-type: disc;
	padding-left: 1em;
}
</style>