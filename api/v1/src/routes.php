<?php

// -----------------------------------------------------------------------------
// Routes
// -----------------------------------------------------------------------------


$app->group('/coverletter', function () {	
	$this->map(['GET'], '', 'LetterController:getAllLetter');
	$this->map(['POST'], '', 'LetterController:insertLetter');

	$this->group('/page/{page:[0-9]+}', function() {
		$this->map(['GET'], '', 'LetterController:paginateLetter');
	});

	$this->group('/{letter_id:[0-9]+}', function() {
		$this->map(['DELETE'], '', 'LetterController:deleteLetter');
		$this->map(['GET'], '', 'LetterController:getAllLetter');
	});

});

$app->group('/coverletter/update', function(){
		$this->map(['POST'], '', 'LetterController:updateLetter');
	});

$app->group('/coverletter/default/{letter_id:[0-9]+}', function(){
		$this->map(['GET'], '', 'LetterController:updateDefaultLetter');
	});