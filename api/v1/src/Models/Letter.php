<?php

namespace App\Models;

use PDO;

class Letter {

    /**
    * @var $db
    */
    private $db;

    public function __construct(PDO $db) {
        $this->db = $db;

    }

    // Get All Letter
    public function getAllLetter($letterID) {

        $whereClause = (!empty($letterID)) ? "AND id = $letterID " : null;

        try {
            $sql = "
                SELECT 
                    id, title, content
                FROM
                    cover_letter
                WHERE
                    is_deleted = 0
                    $whereClause
                ORDER BY
                    date_created DESC
            "; 

            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch(PDOException $e) {
            return $e;
        }
    }

    // Paginate Letters
    public function paginateLetter($page) {
        $page_limit = 3;

        $total = $this->db->prepare('
                SELECT
                    COUNT(*)
                FROM
                    cover_letter
            ')->fetchColumn();

        $pages = ceil($total / $page_limit);

        $offset = ($page - 1)  * $page_limit;

        $start = $offset + 1;
        $end = min(($offset + $page_limit), $total);

        try {
            $statement = $this->db->prepare('
                SELECT 
                    id, title, content, is_default
                FROM
                    cover_letter
                WHERE
                    is_deleted = 0
                ORDER BY
                    is_default DESC
                LIMIT 
                    :limit 
                OFFSET
                    :offset
            '); 

            $statement->bindParam(':limit', $page_limit, PDO::PARAM_INT);
            $statement->bindParam(':offset', $offset, PDO::PARAM_INT);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            return $statement->fetchAll();
        } catch(PDOException $e) {
            return $e;
        }
    }

    // Add Letter
    public function insertLetter($title, $content){
        try {

            $sql = "
                INSERT INTO 
                    cover_letter
                (title, content, is_deleted)
                VALUES
                ('$title', '$content', 0)
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute();

        } catch(PDOException $e) {
            return $e;
        }
    }

    //Update Letter
    public function updateLetter($letterID, $title, $content) {
        try {

            $sql = "
                UPDATE
                    cover_letter
                SET
                    title = '$title',
                    content = '$content'
                WHERE
                    id = $letterID
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute();

        } catch(PDOException $e) {
            return $e->errorInfo();
        }
    }

    // Delete Letter
    public function deleteLetter($letterID) {
        try {

            $sql = "
                UPDATE
                    cover_letter
                SET
                    is_deleted = 1
                WHERE
                    id = $letterID
            ";

            $statement = $this->db->prepare($sql);

            return $statement->execute();

        } catch(PDOException $e) {
            return $e;
        }
    }

    // Set Default Letter
    public function setDefaultLetter($letterID) {

        $sql = "
            SELECT
                id
            FROM
                cover_letter
            WHERE
                id = '$letterID'
            AND 
                is_default = '1'
        ";
        
        $statement = $this->db->prepare($sql);
        
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);

        $results = $statement->fetchAll();    

        if(count($results) === 1) {

            try {

                $sql = "
                    UPDATE
                        cover_letter
                    SET
                        is_default = 0
                    WHERE
                        id = $letterID
                ";

                $statement = $this->db->prepare($sql);

                return $statement->execute();

            } catch(PDOException $e) {
                return $e;
            }

        }else {

            try {

                $sql = "
                    UPDATE
                        cover_letter
                    SET
                        is_default = 1
                    WHERE
                        id = $letterID
                ";

                $statement = $this->db->prepare($sql);

                return $statement->execute();

            } catch(PDOException $e) {
                return $e;
            }

        }

    }

    // Set Default Letter
    public function checkDefaultLetter($letterID) {
        try {

            $sql = "
                   SELECT
                       id
                   FROM
                       cover_letter
                   WHERE
                       is_default = '1'
                   AND
                       is_deleted = 0
               ";
               
            $statement = $this->db->prepare($sql);

            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_ASSOC);

            $results = $statement->fetchAll();  

           
            if(count($results) === 1){
                return $results[0];
            } else {
                return false;
            }

        } catch(PDOException $e) {
            return $e;
        }
    }

}