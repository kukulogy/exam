<?php
// Define Namespace
namespace App\Helpers;

// Model Exam
use App\Models\Letter;

//Class
class Validation {

    protected $letter;

    public function __construct__(Letter $letter) {
        $this->letter = $letter;
    }


    // Validate Title if not null
    public function validateTitle($title) {
        if(empty($title)) {
            return array(false, 'Please enter valid Title.');
        }

        return array(true);
    }

    // Validate Title if not null
    public function validateContent($content) {
        if(empty($content)) {
            return array(false, 'Please enter valid Content.');
        }

        return array(true);
    }

}