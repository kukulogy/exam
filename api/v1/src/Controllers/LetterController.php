<?php
/**
 * Created by Kuku Martirez.
 * Letter: kuku
 * Date: 10/10/16
 */

namespace App\Controllers;

use App\Models\Letter;
use App\Helpers\Validation;
/**
 * Class ExamController
 * @package App\Controllers
 */

class LetterController {

	protected $cletter;
	protected $validation;

	public function __construct(Letter $letter, Validation $validation) {

		// Model
		$this->letter = $letter;

		// Validation Helper
		$this->validation = $validation;

	}

	/**
     * @param $request
     * @param $response
     * @return $response
     */


	//Get All Letter
	public function getAllLetter($request, $response, $args) {

		$letterId = (!empty($args['letter_id'])) ? $args['letter_id'] : NULL;

		$result = $this->letter->getAllLetter($letterId);

		if(empty($result)) {
			return $response->withStatus(200)->withJson(array(
				'success'	=> false
			));
		}

		$output = array(
			'success' => true,
			'data'    => $result,
			'data_len'  => count($result)
		);

		return $response->withStatus(200)->withJson($output);

	}	

	//Set Default Letter
	public function updateDefaultLetter($request, $response, $args) {

		$letterId = (!empty($args['letter_id'])) ? $args['letter_id'] : NULL;

	
		$result = $this->letter->checkDefaultLetter($letterId);

		if($result['id'] != $letterId AND $result){
			return $response->withStatus(200)->withJson(array(
				'success'	=> false
			));
		}

		$result = $this->letter->setDefaultLetter($letterId);

		if(empty($result)) {
			return $response->withStatus(200)->withJson(array(
				'success'	=> false
			));
		}

		$output = array(
			'success' => true,
			'data'    => $result,
		);

		return $response->withStatus(200)->withJson($output);

	}	

	//Paginate Letter
	public function paginateLetter($request, $response, $args) {

		$letterId = (!empty($args['page'])) ? $args['page'] : NULL;

		$result = $this->letter->paginateLetter($letterId);

		if(empty($result)) {
			return $response->withStatus(200)->withJson(array(
				'success'	=> false,
				'data_len'  => count($result)
			));
		}

		$output = array(
			'success' => true,
			'data'    => $result,
			'data_len'  => count($result)
		);

		return $response->withStatus(200)->withJson($output);

	}

	// Add Letter
	public function insertLetter($request, $response, $args) {

		$title   = ucwords($request->getParam('title'));
		$content = $request->getParam('content');

		// SERVER SIDE VALIDATION

		$error = array();

		//Validation Helper
		$ArrTitle = $this->validation->validateTitle($title);
		$ArrContent = $this->validation->validateContent($content);

		if(!$ArrTitle[0]) {
			array_push($error, $ArrTitle[1]);
		}

		if(!$ArrContent[0]) {
			array_push($error, $ArrContent[1]);
		}


		// Return if there's error
		if(!empty($error)) {
			return $response->withStatus(200)->withJson(array(
				'success'	=> false,
				'data'		=> $error,
				'validation'=> false
			));
		}

		// Model Function
		$result = $this->letter->insertLetter($title, $content);

		if(!$result) {
			$output = array(
				'success'	=> false,
				'data'		=> $result
			);
		}

		$output = array(
			'success'	=> true,
			'data'		=> $result
		);

		return $response->withStatus(200)->withJson($output);
	}

	// Update Letter
	public function updateLetter($request, $response, $args) {
		
		$letterID = $request->getParam('letter_id');
		$content  = $request->getParam('content');
		$title    = ucwords($request->getParam('title'));
		// SERVER SIDE VALIDATION

		$error = array();

		//Validation Helper
		$ArrTitle = $this->validation->validateTitle($title);
		$ArrContent = $this->validation->validateContent($content);

		if(!$ArrTitle[0]) {
			array_push($error, $ArrTitle[1]);
		}

		if(!$ArrContent[0]) {
			array_push($error, $ArrContent[1]);
		}

		// Return if there's error
		if(!empty($error)) {
			return $response->withStatus(200)->withJson(array(
				'success'	=> false,
				'data'		=> $error,
				'validation'=> false
			));
		}

		// Model Function
		$result = $this->letter->updateLetter($letterID, $title, $content);

		if(!$result) {
			$output = array(
				'success'	=> false,
				'data'		=> $result
			);
		}

		$output = array(
			'success'	=> true,
			'data'		=> $result
		);

		return $response->withStatus(200)->withJson($output);
	}

	// Deactivate Letter
	public function deleteLetter($request, $response, $args) {

		$letter_id	= $args['letter_id'];

		$result = $this->letter->deleteLetter($letter_id);

		if(!$result) {
			$output = array(
				'success'	=> false,
				'data'		=> $result
			);
		}

		$output = array(
			'success'	=> true,
			'data'		=> $result
		);

		return $response->withStatus(200)->withJson($output);
	}

}